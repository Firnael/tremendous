package com.nael.tremendous.builders;

import com.apollo.Entity;
import com.apollo.EntityBuilder;
import com.apollo.World;
import com.apollo.components.Transform;
import com.nael.tremendous.components.BodyComponent;
import com.nael.tremendous.components.StepOnComponent;
import com.nael.tremendous.components.WarpComponent;
import com.nael.tremendous.spatials.StairsSpatial;

/**
 * Created with IntelliJ IDEA.
 * User: Nael
 * Date: 06/02/13
 * Time: 21:12
 */
public class StairsBuilder implements EntityBuilder {

    @Override
    public Entity buildEntity(World world) {

        final Entity e = new Entity(world);
        e.setComponent(new Transform());
        e.setComponent(new BodyComponent());
        e.setComponent(new WarpComponent());
        e.setComponent(new StepOnComponent());
        e.setComponent(new StairsSpatial());

        return e;
    }
}
