package com.nael.tremendous.builders;

import com.apollo.Entity;
import com.apollo.EntityBuilder;
import com.apollo.World;
import com.apollo.components.Transform;
import com.apollo.managers.GroupManager;
import com.nael.tremendous.Groups;
import com.nael.tremendous.components.BodyComponent;
import com.nael.tremendous.components.IndexComponent;
import com.nael.tremendous.components.TileTypeComponent;
import com.nael.tremendous.spatials.TileSpatial;

/**
 * Created with IntelliJ IDEA.
 * User: Nael
 * Date: 22/11/12
 * Time: 10:09
 */
public class TileBuilder implements EntityBuilder {

    @Override
    public Entity buildEntity(World world) {

        final Entity e = new Entity(world);
        e.setComponent(new Transform());
        e.setComponent(new BodyComponent());
        e.setComponent(new IndexComponent());
        e.setComponent(new TileTypeComponent());
        e.setComponent(new TileSpatial());

        GroupManager groupManager = world.getManager(GroupManager.class);
        groupManager.setGroup(e, Groups.Tiles);

        return e;
    }
}
