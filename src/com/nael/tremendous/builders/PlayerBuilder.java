package com.nael.tremendous.builders;

import com.apollo.Entity;
import com.apollo.EntityBuilder;
import com.apollo.World;
import com.apollo.components.Transform;
import com.apollo.managers.GroupManager;
import com.apollo.managers.TagManager;
import com.nael.tremendous.Groups;
import com.nael.tremendous.Tags;
import com.nael.tremendous.components.BodyComponent;
import com.nael.tremendous.components.MovementComponent;
import com.nael.tremendous.components.OrientationComponent;
import com.nael.tremendous.spatials.PlayerSpatial;

/**
 * Created with IntelliJ IDEA.
 * User: Nael
 * Date: 16/11/12
 * Time: 12:12
 */
public class PlayerBuilder implements EntityBuilder {

    @Override
    public Entity buildEntity(final World world) {

        final Entity e = new Entity(world);
        e.setComponent(new Transform());
        e.setComponent(new BodyComponent());
        e.setComponent(new MovementComponent());
        e.setComponent(new OrientationComponent());
        e.setComponent(new PlayerSpatial());

        world.getManager(TagManager.class).register(Tags.Player, e);

        GroupManager groupManager = world.getManager(GroupManager.class);
        groupManager.setGroup(e, Groups.Actors);

        return e;
    }
}
