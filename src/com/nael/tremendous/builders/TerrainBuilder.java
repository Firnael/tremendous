package com.nael.tremendous.builders;

import com.apollo.Entity;
import com.apollo.EntityBuilder;
import com.apollo.World;
import com.apollo.managers.TagManager;
import com.nael.tremendous.Tags;
import com.nael.tremendous.components.TilemapComponent;

/**
 * Created with IntelliJ IDEA.
 * User: Nael
 * Date: 16/11/12
 * Time: 17:37
 */
public class TerrainBuilder implements EntityBuilder {

    private final int WIDTH  = 30;
    private final int HEIGHT = 30;

    @Override
    public Entity buildEntity(final World world) {

        final Entity e = new Entity(world);
        e.setComponent(new TilemapComponent(WIDTH, HEIGHT));

        world.getManager(TagManager.class).register(Tags.Terrain, e);

        return e;
    }
}
