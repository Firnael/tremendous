/**
 * Created with IntelliJ IDEA.
 * User: Nael
 * Date: 07/02/13
 * Time: 09:29
 */

package com.nael.tremendous.components;

import com.apollo.Component;
import com.apollo.Entity;
import com.apollo.annotate.InjectComponent;
import com.apollo.annotate.InjectManager;
import com.apollo.managers.TagManager;
import com.apollo.utils.Bag;

/*
Component used to determine if something is stepping on the entity,
to simulate a pressure plate behavior
 */

public class StepOnComponent extends Component {

    @InjectComponent
    BodyComponent bodyComponent;

    @InjectManager
    TagManager tagManager;

    private Bag<Entity> entering;
    private Bag<Entity> leaving;
    private Bag<Entity> stepping;
    private boolean someoneIsSteppingOnMe;

    public StepOnComponent() {
        this.entering = new Bag<Entity>();
        this.leaving = new Bag<Entity>();
        this.stepping = new Bag<Entity>();
        this.someoneIsSteppingOnMe = false;
    }

    @Override
    public void update(int delta) {

        // Get player
        Entity player = tagManager.getEntity("Player");

        // Entering and stepping
        if(stepping.contains(player)) {
            //System.out.println("Player stepping");
        }
        else if(isSteppingOnMe(player)) {
            if(entering.contains(player)) {
                entering.remove(player);
                stepping.add(player);
            }
            else {
                entering.add(player);
                System.out.println("Player entering");
            }
        }

        // Leaving (Remove entities which aren't stepping on me anymore)
        leaving.clear();
        for(Entity e : stepping) {
            if(isSteppingOnMe(e) == false) {
                leaving.add(e);
                System.out.println("Entity leaving");
            }
        }
        for(Entity e : leaving) {
            if(stepping.contains(e)) {
                stepping.remove(e);
            }
        }

        // Set the boolean if someone is stepping on the entity
        if(entering.isEmpty() && stepping.isEmpty()) {
            someoneIsSteppingOnMe = false;
        }
        else {
            someoneIsSteppingOnMe = true;
        }
    }

    public boolean isSomeoneSteppingOnMe() {
        return someoneIsSteppingOnMe;
    }

    public Bag<Entity> getEnteringEntities() {
        return entering;
    }

    public Bag<Entity> getSteppingEntities() {
        return stepping;
    }

    public Bag<Entity> getLeavingEntities() {
        return leaving;
    }

    public boolean isSteppingOnMe(Entity e) {
        if(e.getComponent(BodyComponent.class).getBox().intersects(bodyComponent.getBox())) {
            return true;
        }
        return false;
    }
}
