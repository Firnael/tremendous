package com.nael.tremendous.components;

import com.apollo.Component;
import com.apollo.Entity;
import com.apollo.annotate.InjectComponent;
import com.apollo.components.Transform;

/**
 * Created with IntelliJ IDEA.
 * User: Nael
 * Date: 06/02/13
 * Time: 21:22
 */
public class WarpComponent extends Component {

    @InjectComponent
    StepOnComponent stepOnComponent;

    private int floorToWarpTo;


    public WarpComponent() {
        this.floorToWarpTo = -1;
    }

    @Override
    public void update(int delta) {
        for(Entity e : stepOnComponent.getLeavingEntities()) {
            e.getComponent(Transform.class).setLocation(120.f, 120.f);
        }
    }

    public int getFloorToWarpTo() {
        return floorToWarpTo;
    }

    public void setFloorToWarpTo(int nextFloor) {
        floorToWarpTo = nextFloor;
    }


}
