package com.nael.tremendous.components;

import com.apollo.Component;
import com.nael.tremendous.TileType;

/**
 * Created with IntelliJ IDEA.
 * User: Nael
 * Date: 16/11/12
 * Time: 16:56
 */
public class TilemapComponent extends Component {

    private TileType[][] tilemap;
    private int width;
    private int height;
    private final int TILE_WIDTH  = 32;
    private final int TILE_HEIGHT = 32;


    public TilemapComponent(int width, int height) {
        this.width = width;
        this.height = height;
        this.tilemap = new TileType[width][height];

        for(int i=0; i<width; i++) {
            for(int j=0; j<height; j++) {
                this.tilemap[i][j] = TileType.VOID;
            }
        }
    }

    public void setTileTypeAt(int x, int y, TileType tile) {
        tilemap[x][y] = tile;
    }

    public TileType getTileTypeAt(int x, int y) {
        return tilemap[x][y];
    }

    public int getTileWidth() {
        return this.TILE_WIDTH;
    }

    public int getTileHeight() {
        return this.TILE_HEIGHT;
    }

    public int getWidthInTiles() {
        return width;
    }

    public int getHeightInTiles() {
        return height;
    }

    public int getWidthInPixels() {
        return TILE_WIDTH * width;
    }

    public int getHeightInPixels() {
        return TILE_HEIGHT * height;
    }
}
