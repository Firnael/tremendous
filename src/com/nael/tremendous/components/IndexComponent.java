package com.nael.tremendous.components;

import com.apollo.Component;

/**
 * Created with IntelliJ IDEA.
 * User: Nael
 * Date: 22/11/12
 * Time: 10:55
 */
public class IndexComponent extends Component {

    private int x, y;

    public IndexComponent() {
    }

    public IndexComponent(int x, int y) {
        this.x = x;
        this.y = y;
    }

    public int getX() {
        return x;
    }

    public int getY() {
        return y;
    }

    public void setX(int x) {
        this.x = x;
    }

    public void setY(int y) {
        this.y = y;
    }

    public void setIndex(int x, int y) {
        this.x = x;
        this.y = y;
    }
}
