package com.nael.tremendous.components;

import com.apollo.Component;

/**
 * Created with IntelliJ IDEA.
 * User: Nael
 * Date: 17/01/13
 * Time: 15:07
 */
public class OrientationComponent extends Component {

    public enum Orientation {
        N,
        S,
        E,
        W
    }

    private Orientation orientation;

    public OrientationComponent() {
        this.orientation = Orientation.S;
    }

    public OrientationComponent(Orientation initOrientation) {
        this.orientation = initOrientation;
    }

    public Orientation getOrientation() {
        return orientation;
    }

    public void setOrientation(Orientation orientation) {
        this.orientation = orientation;
    }
}
