package com.nael.tremendous.components;

import com.apollo.Component;

/**
 * Created with IntelliJ IDEA.
 * User: Nael
 * Date: 16/11/12
 * Time: 12:09
 */
public class MovementComponent extends Component {

    private float vx, vy;

    public MovementComponent() {
    }

    public MovementComponent(float vx, float vy) {
        this.vx = vx;
        this.vy = vy;
    }

    public void setVectors(float vx, float vy) {
        this.vx = vx;
        this.vy = vy;
    }

    public float getVx() {
        return vx;
    }

    public void setVx(float vx) {
        this.vx = vx;
    }

    public float getVy() {
        return vy;
    }

    public void setVy(float vy) {
        this.vy = vy;
    }

    public boolean isMoving() {
        if(this.vx == 0.f && this.vy == 0.f)
            return true;

        return false;
    }
}
