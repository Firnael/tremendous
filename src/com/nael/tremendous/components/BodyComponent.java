package com.nael.tremendous.components;

import com.apollo.Component;
import com.apollo.annotate.InjectComponent;
import com.apollo.components.Transform;
import org.newdawn.slick.geom.Shape;

/**
 * Created with IntelliJ IDEA.
 * User: Nael
 * Date: 23/11/12
 * Time: 13:25
 */
public class BodyComponent extends Component {

    @InjectComponent
    Transform transformComponent;

    private Shape box;
    private boolean solid;


    public BodyComponent() {
    }

    public BodyComponent(Shape box, boolean solid) {
        this.box = box;
        this.solid = solid;
    }

    @Override
    public void update(int delta) {
        box.setLocation(transformComponent.getX(), transformComponent.getY());
    }

    public Shape getBox() {
        return this.box;
    }

    public void setBox(Shape box) {
        this.box = box;
    }

    public boolean isSolid() {
        return solid;
    }

    public void setSolid(boolean value) {
        this.solid = value;
    }
}
