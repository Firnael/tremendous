package com.nael.tremendous.components;

import com.apollo.Component;
import com.nael.tremendous.TileType;

/**
 * Created with IntelliJ IDEA.
 * User: Nael
 * Date: 30/01/13
 * Time: 17:46
 */
public class TileTypeComponent extends Component {

    private TileType tileType;


    public TileTypeComponent() {
        this.tileType = TileType.VOID;
    }

    public TileTypeComponent(TileType type) {
        this.tileType = type;
    }

    public void setTileType(TileType type) {
        this.tileType = type;
    }

    public TileType getTileType() {
        return this.tileType;
    }

}
