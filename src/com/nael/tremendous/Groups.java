package com.nael.tremendous;

import com.apollo.managers.Group;

/**
 * Created with IntelliJ IDEA.
 * User: Nael
 * Date: 21/11/12
 * Time: 18:17
 */
public enum Groups implements Group {
    Tiles,
    Actors
}
