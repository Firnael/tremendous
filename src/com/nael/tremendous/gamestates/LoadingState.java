package com.nael.tremendous.gamestates;

import com.apollo.World;
import com.apollo.managers.GroupManager;
import com.apollo.managers.TagManager;
import com.nael.tremendous.GameStates;
import com.nael.tremendous.builders.PlayerBuilder;
import com.nael.tremendous.builders.StairsBuilder;
import com.nael.tremendous.builders.TerrainBuilder;
import com.nael.tremendous.builders.TileBuilder;
import com.nael.tremendous.managers.*;
import com.nael.tremendous.utils.ResourceManager;
import com.nael.tremendous.utils.WorldManager;
import org.newdawn.slick.GameContainer;
import org.newdawn.slick.Graphics;
import org.newdawn.slick.SlickException;
import org.newdawn.slick.state.BasicGameState;
import org.newdawn.slick.state.StateBasedGame;

import java.io.FileInputStream;
import java.io.FileNotFoundException;

/**
 * Created with IntelliJ IDEA.
 * User: Audren
 * Date: 31/01/13
 * Time: 13:06
 */
public class LoadingState extends BasicGameState {

    private StateBasedGame game;
    private GameContainer container;
    private boolean resourcesLoaded;
    private boolean worldLoaded;


    @Override
    public void init(GameContainer container, StateBasedGame game) throws SlickException {
        this.game = game;
        this.container = container;
        this.worldLoaded = false;
        this.resourcesLoaded = false;
    }

    @Override
    public void update(GameContainer container, StateBasedGame game, int delta) throws SlickException {

        if(!resourcesLoaded) {
            System.out.println("GAME - Resources loading");
            loadResources();

        } else if(!worldLoaded) {
            System.out.println("GAME - World creation");
            createWorld();
        } else {
            game.enterState(GameStates.GAMEPLAY.ordinal());
        }
    }

    @Override
    public void render(GameContainer container, StateBasedGame game, Graphics g) throws SlickException {
        g.drawString("Loading...", 10, 20);
    }

    private void loadResources() {

        try {
            ResourceManager.getInstance().loadResources(new FileInputStream("data/test.xml"), true);
        } catch (FileNotFoundException e) {
            System.out.println("Resource-loader XML file not found");
        } catch (SlickException e) {
            e.printStackTrace();
        }

        resourcesLoaded = true;
    }

    private void createWorld() {

        World world = WorldManager.getInstance().getWorld();

        world.setManager(new CameraRenderManager(container.getGraphics()));
        world.setManager(new TagManager());
        world.setManager(new GroupManager());
        world.setManager(new CollisionManager(container));
        world.setManager(new MovementManager());
        world.setManager(new PlayerMovementManager(container));
        world.setManager(new CameraManager(container));
        world.setManager(new TilemapsManager());
        world.setManager(new TileManager());
        world.setManager(new BoundaryManager());
        world.setManager(new FloorsManager());
        world.setManager(new DungeonManager());

        world.setEntityBuilder("Player", new PlayerBuilder());
        world.setEntityBuilder("Terrain", new TerrainBuilder());
        world.setEntityBuilder("Tile", new TileBuilder());
        world.setEntityBuilder("Stairs", new StairsBuilder());

        worldLoaded = true;
    }

    @Override
    public int getID() {
        return GameStates.LOADING.ordinal();
    }

    public GameStates getStateName() {
        return GameStates.LOADING;
    }

    @Override
    public void enter(GameContainer container, StateBasedGame stateBasedGame) throws SlickException {
        System.out.println("Entering state " + getStateName());
    }

    @Override
    public void leave(GameContainer container, StateBasedGame stateBasedGame) throws SlickException {
        System.out.println("Leaving state " + getStateName());
    }
}
