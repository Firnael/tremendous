package com.nael.tremendous.gamestates;

import com.apollo.World;
import com.nael.tremendous.GameStates;
import com.nael.tremendous.managers.CameraRenderManager;
import com.nael.tremendous.managers.CollisionManager;
import com.nael.tremendous.managers.DungeonManager;
import com.nael.tremendous.managers.MovementManager;
import com.nael.tremendous.utils.WorldManager;
import org.newdawn.slick.GameContainer;
import org.newdawn.slick.Graphics;
import org.newdawn.slick.SlickException;
import org.newdawn.slick.state.BasicGameState;
import org.newdawn.slick.state.StateBasedGame;

/**
 * Created with IntelliJ IDEA.
 * User: Audren
 * Date: 31/01/13
 * Time: 13:08
 */
public class GameplayState extends BasicGameState {

    private StateBasedGame game;
    private GameContainer container;
    private World world;
    private boolean initiated;


    @Override
    public void init(GameContainer container, StateBasedGame game) throws SlickException {
        this.game = game;
        this.container = container;
        this.world = WorldManager.getInstance().getWorld();
        this.initiated = false;
    }

    @Override
    public void update(GameContainer container, StateBasedGame game, int delta) throws SlickException {

        world.update(delta);
        world.getManager(CollisionManager.class).processCollisions(delta);
        world.getManager(MovementManager.class).processMovements(delta);

        if(!initiated) {
            // Scene creation
            createScene();
            initiated=true;
        }
    }

    @Override
    public void render(GameContainer container, StateBasedGame game, Graphics g) throws SlickException {
        world.getManager(CameraRenderManager.class).render(g);
    }

    private void createScene() {
        DungeonManager dm = world.getManager(DungeonManager.class);
        dm.createEntranceFloor();
        dm.setActiveFloor(0);
        dm.createPlayer();
    }

    @Override
    public int getID() {
        return GameStates.GAMEPLAY.ordinal();
    }

    public GameStates getStateName() {
        return GameStates.GAMEPLAY;
    }

    @Override
    public void enter(GameContainer container, StateBasedGame stateBasedGame) throws SlickException {
        System.out.println("Entering state " + getStateName());
    }

    @Override
    public void leave(GameContainer container, StateBasedGame stateBasedGame) throws SlickException {
        System.out.println("Leaving state " + getStateName());
    }
}
