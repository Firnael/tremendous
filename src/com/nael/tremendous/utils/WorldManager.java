package com.nael.tremendous.utils;

import com.apollo.World;
import org.newdawn.slick.Image;
import org.newdawn.slick.Sound;

import java.util.HashMap;
import java.util.Map;

/**
 * Created with IntelliJ IDEA.
 * User: Audren
 * Date: 31/01/13
 * Time: 13:17
 */
public class WorldManager {

    private static WorldManager _instance = new WorldManager();
    private World world;

    private WorldManager() {
        world = new World();
    }

    public final static WorldManager getInstance() {
        return _instance;
    }

    public World getWorld() {
        return world;
    }
}
