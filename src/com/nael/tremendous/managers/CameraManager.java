package com.nael.tremendous.managers;

import com.apollo.Entity;
import com.apollo.annotate.InjectManager;
import com.apollo.components.Transform;
import com.apollo.managers.Manager;
import com.apollo.managers.TagManager;
import com.nael.tremendous.Tags;
import org.newdawn.slick.GameContainer;
import org.newdawn.slick.Input;
import org.newdawn.slick.MouseListener;
import org.newdawn.slick.geom.Vector2f;

/**
 * Created with IntelliJ IDEA.
 * User: Nael
 * Date: 21/11/12
 * Time: 12:27
 */
public class CameraManager extends Manager implements MouseListener {

    private GameContainer container;
    private float lookAtX;
    private float lookAtY;
    private int screenWidth;
    private int screenHeight;
    private Vector2f zoomFactor;
    private Input input;

    @InjectManager
    BoundaryManager boundaryManager;


    public CameraManager(GameContainer container) {
        this.container = container;
        this.zoomFactor = new Vector2f(1.f, 1.f);
    }

    @Override
    public void initialize() {

        System.out.println("MANAGER - Initializing CameraManager");

        input = container.getInput();
        input.addMouseListener(this);

        screenWidth = container.getWidth();
        screenHeight = container.getHeight();
    }

    @Override
    public void update(int delta) {

        Entity player = world.getManager(TagManager.class).getEntity(Tags.Player);

        if(player != null) {

            lookAtX = player.getComponent(Transform.class).getX();
            lookAtY = player.getComponent(Transform.class).getY();

            //fixBoundaries();

            input.setOffset(getStartX(), getStartY());
        }
    }

    private void fixBoundaries() {

        if (getEndX() > boundaryManager.getBoundaryWidth()) {
            lookAtX -= getEndX() - boundaryManager.getBoundaryWidth();
        } else if (getStartX() < 0) {
            lookAtX -= getStartX();
        }

        if (getEndY() > boundaryManager.getBoundaryHeight()) {
            lookAtY -= getEndY() - boundaryManager.getBoundaryHeight();
        } else if (getStartY() < 0) {
            lookAtY -= getStartY();
        }
    }

    private float getOffsetX() {
        return ((float) screenWidth) / 2f;
    }

    private float getOffsetY() {
        return ((float) screenHeight) / 2f;
    }

    public float getStartX() {
        return lookAtX - getOffsetX();
    }

    public float getStartY() {
        return lookAtY - getOffsetY();
    }

    public float getEndX() {
        return lookAtX + getOffsetX();
    }

    public float getEndY() {
        return lookAtY + getOffsetY();
    }

    public float getWidth() {
        return getEndX() - getStartX();
    }

    public float getHeight() {
        return getEndY() - getStartY();
    }

    public void setZoomFactor(Vector2f newZoomFactor) {
        zoomFactor = newZoomFactor;
    }

    public Vector2f getZoomFactor() {
        return zoomFactor;
    }


    // === MouseListener implementation

    @Override
    public void mouseWheelMoved(int i) {
        // i<0 => mouse wheel down
        if(i<0) {
            this.setZoomFactor(new Vector2f(zoomFactor.x - 0.1f, zoomFactor.y - 0.1f));
        }
        // i>0 => mouse wheel up
        else {
            this.setZoomFactor(new Vector2f(zoomFactor.x + 0.1f, zoomFactor.y + 0.1f));
        }
    }

    @Override
    public void mouseClicked(int i, int i1, int i2, int i3) {
        // i=0 => left button
        // i=1 => right button
        // i=2 => wheel button
        if(i==2) {
            this.setZoomFactor(new Vector2f(1.f, 1.f));
        }
    }

    @Override
    public void mousePressed(int i, int i1, int i2) {
    }

    @Override
    public void mouseReleased(int i, int i1, int i2) {
    }

    @Override
    public void mouseMoved(int i, int i1, int i2, int i3) {
    }

    @Override
    public void mouseDragged(int i, int i1, int i2, int i3) {
    }

    @Override
    public void setInput(Input input) {
    }

    @Override
    public boolean isAcceptingInput() {
        return true;
    }

    @Override
    public void inputEnded() {
    }

    @Override
    public void inputStarted() {
    }
}
