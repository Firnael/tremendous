package com.nael.tremendous.managers;

import com.apollo.Entity;
import com.apollo.components.Transform;
import com.apollo.managers.EntityManager;
import com.apollo.managers.Manager;
import com.apollo.utils.Bag;
import com.nael.tremendous.components.MovementComponent;

/**
 * Created with IntelliJ IDEA.
 * User: Nael
 * Date: 10/01/13
 * Time: 08:59
 */
public class MovementManager extends Manager {

    private Bag<Entity> dynamicsEntities;

    @Override
    public void initialize() {
        System.out.println("MANAGER - Initializing MovementManager");
    }

    @Override
    public void update(int delta) {
        EntityManager em = world.getEntityManager();
        dynamicsEntities = em.getEntitiesByComponentType(MovementComponent.class);
    }

    public void processMovements(int delta) {

        for(Entity e : dynamicsEntities) {

            Transform transform = e.getComponent(Transform.class);
            MovementComponent movement = e.getComponent(MovementComponent.class);
            transform.addX(delta * movement.getVx());
            transform.addY(delta * movement.getVy());
        }
    }
}
