package com.nael.tremendous.managers;

import com.apollo.annotate.InjectManager;
import com.apollo.managers.RenderManager;
import org.newdawn.slick.Color;
import org.newdawn.slick.Graphics;

/**
 * Created with IntelliJ IDEA.
 * User: Nael
 * Date: 21/11/12
 * Time: 12:56
 */
public class CameraRenderManager extends RenderManager<Graphics> {

    @InjectManager
    CameraManager cameraManager;

    public CameraRenderManager(Graphics graphics) {
        super(graphics);
    }

    @Override
    public void initialize() {
        System.out.println("MANAGER - Initializing CameraRenderManager");
    }

    @Override
    public void render(Graphics graphics) {

        if(cameraManager != null) {

            graphics.translate(-cameraManager.getStartX(), -cameraManager.getStartY());
            graphics.scale(cameraManager.getZoomFactor().x, cameraManager.getZoomFactor().y);
            super.render(graphics);
            graphics.resetTransform();

            graphics.setColor(Color.white);
            graphics.drawString("Active entities: " + world.getEntityManager().getEntities().size(), 10, 25);

            /*
            Entity player = world.getManager(TagManager.class).getEntity(Tags.Player);

            if(player != null) {
                graphics.drawString("Player position : x = "
                        + world.getManager(TagManager.class).getEntity(Tags.Player).getComponent(Transform.class).getX()
                        + ", y = "
                        + world.getManager(TagManager.class).getEntity(Tags.Player).getComponent(Transform.class).getY(),
                        10, 35);

                graphics.drawString("Player speed : vx = "
                        + world.getManager(TagManager.class).getEntity(Tags.Player).getComponent(MovementComponent.class).getVx()
                        + ", vy = "
                        + world.getManager(TagManager.class).getEntity(Tags.Player).getComponent(MovementComponent.class).getVy(),
                        10, 35);

                graphics.drawString("Player orientation : "
                        + world.getManager(TagManager.class).getEntity(Tags.Player)
                        .getComponent(OrientationComponent.class).getOrientation(), 10, 35);
            }
            */
        }
    }
}
