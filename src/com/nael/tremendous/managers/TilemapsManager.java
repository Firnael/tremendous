package com.nael.tremendous.managers;

import com.apollo.Entity;
import com.apollo.managers.Manager;
import com.nael.tremendous.TileType;
import com.nael.tremendous.components.TilemapComponent;

import java.util.ArrayList;

/**
 * Created with IntelliJ IDEA.
 * User: Nael
 * Date: 30/01/13
 * Time: 12:47
 */
public class TilemapsManager extends Manager {

    private ArrayList<Entity> tilemaps;


    public TilemapsManager() {
        this.tilemaps = new ArrayList<Entity>();
    }

    @Override
    public void initialize() {
        System.out.println("MANAGER - Initializing TilemapsManager");
    }

    @Override
    public void update(int delta) {
    }

    public Entity generateEntranceFloorTilemap() {

        int terrainWidth = 12;
        int terrainHeight = 12;

        // Terrain entity creation
        Entity terrain = new Entity(world);
        TilemapComponent tmc = new TilemapComponent(terrainWidth, terrainHeight);
        terrain.setComponent(tmc);
        world.addEntity(terrain);

        // Build the tilemap
        for(int i=0; i<terrainWidth; i++) {
            for(int j=0; j<terrainHeight; j++) {

                if(i==0 || j==0 || i == (terrainWidth-1) || j == (terrainHeight-1))
                {
                    tmc.setTileTypeAt(i, j, TileType.WALL);
                }
                else
                {
                    tmc.setTileTypeAt(i, j, TileType.GRASS);
                }
            }
        }

        //TODO remove
        tmc.setTileTypeAt(9, 9, TileType.VOID);

        tilemaps.add(terrain);

        return terrain;
    }

    public Entity generateNormalFloorTilemap() {
        System.out.println("TilemapsManager.generateNormalFloorTilemap() not implemented yet");
        return null;
    }

    public Entity getTerrain(int index) {
        return this.tilemaps.get(index);
    }

    public TilemapComponent getTilemap(int index) {

        Entity terrain = tilemaps.get(index);
        return terrain.getComponent(TilemapComponent.class);
    }

    public TileType getTileTypeAt(int index, int x, int y) {

        TilemapComponent tmc = getTilemap(index);
        return tmc.getTileTypeAt(x, y);
    }
}
