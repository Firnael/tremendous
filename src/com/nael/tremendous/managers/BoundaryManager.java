package com.nael.tremendous.managers;

import com.apollo.Entity;
import com.apollo.components.Transform;
import com.apollo.managers.EntityManager;
import com.apollo.managers.Manager;
import com.apollo.utils.Bag;
import com.nael.tremendous.components.BodyComponent;
import com.nael.tremendous.components.MovementComponent;
import org.newdawn.slick.geom.Shape;

/**
 * Created with IntelliJ IDEA.
 * User: Nael
 * Date: 21/11/12
 * Time: 16:52
 */
public class BoundaryManager extends Manager {

    private int boundsStartX;
    private int boundsStartY;
    private int boundsEndX;
    private int boundsEndY;

    public BoundaryManager() {
        this.boundsStartX = -1;
        this.boundsStartY = -1;
        this.boundsEndX = -1;
        this.boundsEndY = -1;
    }

    @Override
    public void initialize() {
        System.out.println("MANAGER - Initializing BoundaryManager");
    }

    @Override
    public void update(int delta) {

        EntityManager entityManager = world.getEntityManager();
        Bag entitiesHavingMovement = entityManager.getEntitiesByComponentType(MovementComponent.class);

        for(Object entity : entitiesHavingMovement) {

            Entity e = (Entity)entity;
            Transform t = e.getComponent(Transform.class);
            Shape box = e.getComponent(BodyComponent.class).getBox();

            if(box.getMinX() < boundsStartX) {
                t.setX(boundsStartX);
            }
            if(box.getMaxX() > boundsEndX) {
                t.setX(boundsEndX - box.getWidth());
            }
            if(box.getMinY() < boundsStartY) {
                t.setY(boundsStartY);
            }
            if(box.getMaxY() > boundsEndY) {
                t.setY(boundsEndY - box.getHeight());
            }
        }
    }

    public void changeBoundaries(int boundsStartX, int boundsStartY, int boundsEndX, int boundsEndY) {
        this.boundsStartX = boundsStartX;
        this.boundsStartY = boundsStartY;
        this.boundsEndX = boundsEndX;
        this.boundsEndY = boundsEndY;
    }

    public int getBoundsEndX() {
        return boundsEndX;
    }

    public int getBoundsEndY() {
        return boundsEndY;
    }

    public int getBoundsStartX() {
        return boundsStartX;
    }

    public int getBoundsStartY() {
        return boundsStartY;
    }

    public int getBoundaryWidth() {
        return boundsEndX-boundsStartX;
    }

    public int getBoundaryHeight() {
        return boundsEndY-boundsStartY;
    }
}
