package com.nael.tremendous.managers;

import com.apollo.Entity;
import com.apollo.annotate.InjectManager;
import com.apollo.components.Transform;
import com.apollo.managers.EntityManager;
import com.apollo.managers.GroupManager;
import com.apollo.managers.Manager;
import com.apollo.managers.TagManager;
import com.apollo.utils.Bag;
import com.nael.tremendous.Groups;
import com.nael.tremendous.Tags;
import com.nael.tremendous.components.BodyComponent;
import com.nael.tremendous.components.MovementComponent;
import com.nael.tremendous.components.WarpComponent;
import org.newdawn.slick.GameContainer;
import org.newdawn.slick.geom.Shape;

/**
 * Created with IntelliJ IDEA.
 * User: Nael
 * Date: 23/11/12
 * Time: 14:17
 */
public class CollisionManager extends Manager {

    @InjectManager
    GroupManager groupManager;

    @InjectManager
    TagManager tagManager;

    private GameContainer container;
    private Bag<Entity> tilesBag;
    private Bag<Entity> solidTilesBag;
    private Entity playerEntity;

    public CollisionManager(GameContainer container) {
        this.container = container;
    }

    @Override
    public void initialize() {
        System.out.println("MANAGER - Initializing CollisionManager");
    }

    @Override
    public void update(int delta) {
        playerEntity = tagManager.getEntity(Tags.Player);
        tilesBag = groupManager.getEntityGroup(Groups.Tiles);
    }

    public void processCollisions(int delta) {

        if(playerEntity != null) {

            // Player collide with tiles
            processTileCollisions(delta);

            // Player collides with warps
            processWarpCollisions();
        }
    }

    private void processTileCollisions(int delta) {

        // Get all solid tiles
        solidTilesBag = new Bag<Entity>();

        for(Entity tile : tilesBag) {
            if(getEntitySolidity(tile)) {
                solidTilesBag.add(tile);
            }
        }

        // Get player box
        Shape playerBox = getEntityBox(playerEntity);

        // Test collision with new position
        Transform transform = playerEntity.getComponent(Transform.class);
        MovementComponent movement = playerEntity.getComponent(MovementComponent.class);

        float vx = delta * movement.getVx();
        float vy = delta * movement.getVy();

        for(Entity tile : solidTilesBag) {

            Shape tileBox = getEntityBox(tile);

            // X
            if(vx != 0) {

                transform.addX(vx);
                playerBox.setX(transform.getX());

                if(this.areColliding(tileBox, playerBox)) {
                    movement.setVx(0.f);
                }

                transform.addX(-vx);
                playerBox.setX(transform.getX());
            }

            // Y
            if(vy != 0) {

                transform.addY(vy);
                playerBox.setY(transform.getY());

                if(this.areColliding(tileBox, playerBox)) {
                    movement.setVy(0.f);
                }

                transform.addY(-vy);
                playerBox.setY(transform.getY());
            }
        }
    }

    private void processWarpCollisions() {

        // Get all warps
        Bag<Entity> warps = world.getManager(EntityManager.class).getEntitiesByComponentType(WarpComponent.class);

        // Get player box
        Shape playerBox = getEntityBox(playerEntity);

        for(Entity warp : warps) {

            // Get this warp box
            Shape warpBox = getEntityBox(warp);

            if(areColliding(warpBox, playerBox)) {
                //System.out.println("Warp to floor " + warp.getComponent(WarpComponent.class).getFloorToWarpTo());
                break;
            }
        }
    }

    private boolean areColliding(Shape one, Shape two) {
        if(one.intersects(two))
            return true;

        return false;
    }

    private Shape getEntityBox(Entity e) {
        return e.getComponent(BodyComponent.class).getBox();
    }

    private boolean getEntitySolidity(Entity e) {
        return e.getComponent(BodyComponent.class).isSolid();
    }
}
