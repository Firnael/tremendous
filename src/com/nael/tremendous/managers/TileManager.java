package com.nael.tremendous.managers;

import com.apollo.Entity;
import com.apollo.managers.Manager;
import com.nael.tremendous.TileType;
import com.nael.tremendous.components.BodyComponent;
import com.nael.tremendous.components.TileTypeComponent;

/**
 * Created with IntelliJ IDEA.
 * User: Nael
 * Date: 23/11/12
 * Time: 14:59
 */
public class TileManager extends Manager {

    public TileManager() {
    }

    @Override
    public void initialize() {
        System.out.println("MANAGER - Initializing TileManager");
    }

    @Override
    public void update(int delta) {
    }

    public void setTileProperties(Entity tile, TileType type) {

        BodyComponent bc = tile.getComponent(BodyComponent.class);
        TileTypeComponent ttc = tile.getComponent(TileTypeComponent.class);

        ttc.setTileType(type);

        switch(type) {

            case WALL:
                bc.setSolid(true);
                break;
            case GRASS:
                bc.setSolid(false);
                break;
            default:
                bc.setSolid(true);
                break;
        }
    }
}
