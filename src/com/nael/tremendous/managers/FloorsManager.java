package com.nael.tremendous.managers;

import com.apollo.Entity;
import com.apollo.annotate.InjectManager;
import com.apollo.components.Transform;
import com.apollo.managers.Manager;
import com.apollo.utils.Bag;
import com.nael.tremendous.components.*;
import org.newdawn.slick.geom.Rectangle;

/**
 * Created with IntelliJ IDEA.
 * User: Nael
 * Date: 24/01/13
 * Time: 12:30
 */
public class FloorsManager extends Manager {

    @InjectManager
    TilemapsManager tilemapsManager;

    @InjectManager
    TileManager tileManager;


    public FloorsManager() {
    }

    @Override
    public void initialize() {
        System.out.println("MANAGER - Initializing FloorsManager");
    }

    @Override
    public void update(int delta) {
    }

    public Bag<Entity> generateEntranceFloor() {

        Bag<Entity> floor = new Bag<Entity>();

        // Generate terrain entity, with tilemap component
        Entity terrain = tilemapsManager.generateEntranceFloorTilemap();
        TilemapComponent tmc = terrain.getComponent(TilemapComponent.class);

        // Generate tiles entities based on tilemap
        for(int i=0; i<tmc.getWidthInTiles(); i++) {
            for(int j=0; j<tmc.getHeightInTiles(); j++) {

                Entity tile = world.createEntity("Tile");
                world.addEntity(tile);

                int w = 32, h = 32;
                tile.getComponent(Transform.class).setLocation((float) i*w, (float) j*h);
                tile.getComponent(BodyComponent.class).setBox(new Rectangle((float) i*w, (float) j*h, w, h));
                tile.getComponent(IndexComponent.class).setIndex(i, j);

                tileManager.setTileProperties(tile, tmc.getTileTypeAt(i, j));

                floor.add(tile);
            }
        }

        // Generate stairs to next floor
        Entity stairsDown = world.createEntity("Stairs");
        world.addEntity(stairsDown);

        float posX = 160, posY = 32;
        stairsDown.getComponent(Transform.class).setLocation(posX, posY);
        stairsDown.getComponent(BodyComponent.class).setBox(new Rectangle(posX, posY, 32, 32));
        stairsDown.getComponent(WarpComponent.class).setFloorToWarpTo(1);

        floor.add(stairsDown);

        return floor;
    }

    public Bag<Entity> generateNormalFloor() {

        Bag<Entity> floor = new Bag<Entity>();
        return floor;
    }
}
