package com.nael.tremendous.managers;

import com.apollo.Entity;
import com.apollo.managers.Manager;
import com.apollo.managers.TagManager;
import com.nael.tremendous.Tags;
import com.nael.tremendous.components.MovementComponent;
import com.nael.tremendous.components.OrientationComponent;
import org.newdawn.slick.GameContainer;
import org.newdawn.slick.Input;

/**
 * Created with IntelliJ IDEA.
 * User: Nael
 * Date: 16/11/12
 * Time: 14:30
 */
public class PlayerMovementManager extends Manager {

    private GameContainer container;

    public PlayerMovementManager(GameContainer container) {
        this.container = container;
    }

    @Override
    public void initialize() {
        System.out.println("MANAGER - Initializing PlayerMovementManager");
    }

    @Override
    public void update(int delta) {

        Input input = container.getInput();
        Entity player = world.getManager(TagManager.class).getEntity(Tags.Player);

        if(player != null) {

            // Horizontal
            if(input.isKeyDown(Input.KEY_LEFT)) {
                player.getComponent(MovementComponent.class).setVx(-0.1f);
                player.getComponent(OrientationComponent.class).setOrientation(OrientationComponent.Orientation.W);
            }
            else if(input.isKeyDown(Input.KEY_RIGHT)) {
                player.getComponent(MovementComponent.class).setVx(0.1f);
                player.getComponent(OrientationComponent.class).setOrientation(OrientationComponent.Orientation.E);
            }
            else {
                player.getComponent(MovementComponent.class).setVx(0.f);
            }

            // Vertical
            if(input.isKeyDown(Input.KEY_UP)) {
                player.getComponent(MovementComponent.class).setVy(-0.1f);
                player.getComponent(OrientationComponent.class).setOrientation(OrientationComponent.Orientation.N);
            }
            else if(input.isKeyDown(Input.KEY_DOWN)) {
                player.getComponent(MovementComponent.class).setVy(0.1f);
                player.getComponent(OrientationComponent.class).setOrientation(OrientationComponent.Orientation.S);
            }
            else {
                player.getComponent(MovementComponent.class).setVy(0.f);
            }
        }
    }
}
