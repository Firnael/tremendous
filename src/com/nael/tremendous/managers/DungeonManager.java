package com.nael.tremendous.managers;

import com.apollo.Entity;
import com.apollo.annotate.InjectManager;
import com.apollo.components.Transform;
import com.apollo.managers.Manager;
import com.apollo.utils.Bag;
import com.nael.tremendous.components.BodyComponent;
import com.nael.tremendous.components.TilemapComponent;
import org.newdawn.slick.geom.Rectangle;

import java.util.ArrayList;

/**
 * Created with IntelliJ IDEA.
 * User: Nael
 * Date: 24/01/13
 * Time: 12:26
 */
public class DungeonManager extends Manager {

    @InjectManager
    TilemapsManager tilemapsManager;

    @InjectManager
    FloorsManager floorsManager;

    private ArrayList<Bag<Entity>> floors;
    private int activeFloor;


    public DungeonManager() {
        this.floors = new ArrayList<Bag<Entity>>();
        this.activeFloor = -1;
    }

    @Override
    public void initialize() {
        System.out.println("MANAGER - Initializing DungeonManager");
    }

    public void setActiveFloor(int newActiveFloor) {

        // Set new active floor
        activeFloor = newActiveFloor;

        // Define new boundaries defined by this floor
        TilemapComponent tmc = tilemapsManager.getTilemap(activeFloor);
        int w = tmc.getWidthInPixels();
        int h = tmc.getHeightInPixels();

        world.getManager(BoundaryManager.class).changeBoundaries(0, 0, w, h);
    }

    public int getActiveFloor() {
        return this.activeFloor;
    }

    public void createEntranceFloor() {

        if(floors.size() > 0) {
            System.out.println("This dungeon already has an Entrance Floor !");
        }
        else {
            Bag<Entity> entranceFloorEntities = floorsManager.generateEntranceFloor();
            floors.add(entranceFloorEntities);
        }
    }

    public void createNormalFloor() {

        if(floors.size() == 0) {
            System.out.println("You can't add a floor yet, this dungeon has no Entrance Floor !");
        }
        else {
            Bag<Entity> floorEntities = floorsManager.generateNormalFloor();
            floors.add(floorEntities);
        }
    }

    public void createPlayer() {
        try {
            Entity player = world.createEntity("Player");
            player.getComponent(Transform.class).setLocation(40, 40);
            player.getComponent(BodyComponent.class).setBox(new Rectangle(39, 39, 31, 31));
            player.getComponent(BodyComponent.class).setSolid(true);
            world.addEntity(player);
        } catch(Exception e) {
            System.out.println("Failed to create Player");
        }
    }

    public Bag<Entity> getFloorEntities(int floor) {

        if(floors.get(floor) != null) {
            return floors.get(floor);
        }
        return null;
    }

    public Bag<Entity> getActiveFloorEntities() {
        return getFloorEntities(activeFloor);
    }
}
