package com.nael.tremendous;

/**
 * Created with IntelliJ IDEA.
 * User: Nael
 * Date: 30/01/13
 * Time: 12:45
 */
public enum TileType {
    VOID,
    WALL,
    GRASS
}
