package com.nael.tremendous;

import com.nael.tremendous.gamestates.GameplayState;
import com.nael.tremendous.gamestates.LoadingState;
import org.newdawn.slick.AppGameContainer;
import org.newdawn.slick.GameContainer;
import org.newdawn.slick.SlickException;
import org.newdawn.slick.state.StateBasedGame;

/**
 * Created with IntelliJ IDEA.
 * User: Audren
 * Date: 15/11/12
 * Time: 20:01
 */
public class Tremendous extends StateBasedGame {

    public static final int WIDTH = 800;
    public static final int HEIGHT = 600;

    @Override
    public void initStatesList(GameContainer container) throws SlickException {
        this.addState(new LoadingState());
        this.addState(new GameplayState());
    }

    public Tremendous() {
        super("Tremendous");
    }

    public static void main(String[] args) throws SlickException {
        AppGameContainer container = new AppGameContainer(new Tremendous());
        container.setDisplayMode(Tremendous.WIDTH, Tremendous.HEIGHT, false);
        //container.setTargetFrameRate(60);
        //container.setMinimumLogicUpdateInterval(1);
        //container.setMaximumLogicUpdateInterval(1);
        container.setVSync(false);
        container.start();
        container.getGraphics().setAntiAlias(false);

        System.out.println("GAME - Game started");
    }
}
