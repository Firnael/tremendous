package com.nael.tremendous.spatials;

import com.apollo.Layer;

/**
 * Created with IntelliJ IDEA.
 * User: Audren
 * Date: 15/11/12
 */
public enum Layers implements Layer {
    Terrain,
    Mechanisms,
    Actors,
    Interface;

    @Override
    public int getLayerId() {
        return ordinal();
    }
}
