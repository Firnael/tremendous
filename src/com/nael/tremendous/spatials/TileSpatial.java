package com.nael.tremendous.spatials;

import com.apollo.Entity;
import com.apollo.Layer;
import com.apollo.annotate.InjectComponent;
import com.apollo.annotate.InjectManager;
import com.apollo.components.Transform;
import com.nael.tremendous.components.IndexComponent;
import com.nael.tremendous.components.TileTypeComponent;
import com.nael.tremendous.managers.CameraManager;
import com.nael.tremendous.managers.DungeonManager;
import com.nael.tremendous.utils.ResourceManager;
import org.newdawn.slick.Graphics;
import org.newdawn.slick.Image;
import org.newdawn.slick.SpriteSheet;

/**
 * Created with IntelliJ IDEA.
 * User: Nael
 * Date: 22/11/12
 * Time: 10:18
 */
public class TileSpatial extends SlickSpatial {

    @InjectManager
    CameraManager cameraManager;

    @InjectManager
    DungeonManager dungeonManager;

    @InjectComponent
    TileTypeComponent tileTypeComponent;

    @InjectComponent
    Transform transform;

    @InjectComponent
    IndexComponent indexComponent;


    private SpriteSheet spriteSheet;


    public TileSpatial() {
        this.spriteSheet = new SpriteSheet(ResourceManager.getInstance().getImage("TILES"), 32, 32);
    }

    @Override
    public void initialize() {
    }

    @Override
    public void update(int delta) {
    }

    @Override
    public void render(Graphics g) {

        Entity e = this.owner;
        if(dungeonManager.getActiveFloorEntities().contains(e)) {

            int startX = (int)cameraManager.getStartX() / 32;
            int startY = (int)cameraManager.getStartY() / 32;
            int endX = (int)(cameraManager.getEndX() / 32) + 1;
            int endY = (int)(cameraManager.getEndY() / 32) + 1;

            if(indexComponent.getX() >= startX
                    && indexComponent.getX() <= endX
                    && indexComponent.getY() >= startY
                    && indexComponent.getY() <= endY) {

                this.getSpriteByTileType().draw(transform.getX(), transform.getY());
            }
        }
    }

    @Override
    public Layer getLayer() {
        return Layers.Terrain;
    }

    private Image getSpriteByTileType() {

        int x=0, y=0;

        switch(tileTypeComponent.getTileType()){
            case VOID:  x=0; y=0; break;
            case WALL:  x=1; y=0; break;
            case GRASS: x=2; y=0; break;
        }

        return spriteSheet.getSprite(x, y);
    }
}