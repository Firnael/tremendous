package com.nael.tremendous.spatials;

import com.apollo.components.spatial.Spatial;
import org.newdawn.slick.Graphics;

/**
 * Created with IntelliJ IDEA.
 * User: Audren
 * Date: 15/11/12
 * Time: 21:19
 */
public abstract class SlickSpatial extends Spatial<Graphics> {

    public abstract void render(Graphics g);
}
