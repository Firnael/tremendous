package com.nael.tremendous.spatials;

import com.apollo.Layer;
import com.apollo.annotate.InjectComponent;
import com.apollo.components.Transform;
import com.nael.tremendous.components.MovementComponent;
import com.nael.tremendous.components.OrientationComponent;
import com.nael.tremendous.utils.ResourceManager;
import org.newdawn.slick.Animation;
import org.newdawn.slick.Graphics;

/**
 * Created with IntelliJ IDEA.
 * User: Nael
 * Date: 16/11/12
 * Time: 11:59
 */
public class PlayerSpatial extends SlickSpatial {

    @InjectComponent
    Transform transform;

    @InjectComponent
    MovementComponent movement;

    @InjectComponent
    OrientationComponent orientation;

    private Animation anim_walk_up;
    private Animation anim_walk_down;
    private Animation anim_walk_left;
    private Animation anim_walk_right;


    public PlayerSpatial() {
        this.anim_walk_up = ResourceManager.getInstance().getAnimation("PLAYER_WALK_UP");
        this.anim_walk_down = ResourceManager.getInstance().getAnimation("PLAYER_WALK_DOWN");
        this.anim_walk_left = ResourceManager.getInstance().getAnimation("PLAYER_WALK_LEFT");
        this.anim_walk_right = ResourceManager.getInstance().getAnimation("PLAYER_WALK_RIGHT");
    }

    @Override
    public void initialize() {
        System.out.println("SPATIAL - Initializing PlayerSpatial");
    }

    @Override
    public void render(Graphics g) {

        OrientationComponent.Orientation playerOrientation = orientation.getOrientation();
        Animation animToDraw = null;

        switch(playerOrientation) {

            case N: animToDraw = anim_walk_up; break;
            case S: animToDraw = anim_walk_down; break;
            case W: animToDraw = anim_walk_left; break;
            case E: animToDraw = anim_walk_right; break;
        }

        if(movement.isMoving()) {
            animToDraw.setCurrentFrame(0);
            animToDraw.stop();
        }
        else {
            if(animToDraw.isStopped()) {
                animToDraw.restart();
            }
        }

        animToDraw.draw(transform.getX(), transform.getY());
    }

    @Override
    public Layer getLayer() {
        return Layers.Actors;
    }
}
