package com.nael.tremendous.spatials;

import com.apollo.Entity;
import com.apollo.Layer;
import com.apollo.annotate.InjectComponent;
import com.apollo.annotate.InjectManager;
import com.apollo.components.Transform;
import com.nael.tremendous.managers.DungeonManager;
import com.nael.tremendous.utils.ResourceManager;
import org.newdawn.slick.Graphics;
import org.newdawn.slick.Image;

/**
 * Created with IntelliJ IDEA.
 * User: Nael
 * Date: 06/02/13
 * Time: 20:55
 */
public class StairsSpatial extends SlickSpatial {

    @InjectManager
    DungeonManager dungeonManager;

    @InjectComponent
    Transform transform;

    private Image img_stairs;


    public StairsSpatial() {
        this.img_stairs = ResourceManager.getInstance().getImage("STAIRS");
    }

    @Override
    public void initialize() {
        System.out.println("SPATIAL - Initializing StairsSpatial");
    }

    @Override
    public void render(Graphics g) {

        Entity e = this.owner;
        if(dungeonManager.getActiveFloorEntities().contains(e)) {
            img_stairs.draw(transform.getX(), transform.getY());
        }
    }

    @Override
    public Layer getLayer() {
        return Layers.Mechanisms;
    }
}
